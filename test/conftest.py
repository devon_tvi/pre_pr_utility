"""
Test assets lookups and config fixtures
"""

EXAMPLE_DIFF_PATH = "assets/example_diff.txt"


def get_example_diff_contents() -> str:
    """
    Read the contents stored in this asset
    """

    with open(EXAMPLE_DIFF_PATH, "r") as file:
        return file.read()
