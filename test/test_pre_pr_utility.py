"""Tests for `pre_pr_utility` package."""

from test.conftest import get_example_diff_contents

import pre_pr_utility.pre_pr_utility as util


def test_pre_pr_utility() -> None:

    contents = get_example_diff_contents()

    util.main(["-d", contents])
