"""Main module."""

import argparse
import os
import sys
from typing import List, Optional, Tuple


def parse_args(args: List[str] = None) -> argparse.Namespace:
    """ Command line argument parser. """

    parser = argparse.ArgumentParser(description="Pre PR Utility")

    parser.add_argument(
        "-d", "--input-diff", required=True, help="The input diff of two branches"
    )

    return parser.parse_args(args)


def look_for_issues(diff_input: str) -> Tuple[bool, Optional[str]]:
    """

    :param diff_input:
    :return:
    """

    problem_strings = ["print(", "TODO"]

    diff_lines = diff_input.split(os.linesep)
    newly_added_lines = [line for line in diff_lines if line.startswith("+")]

    # search each new line for bad content
    for line in newly_added_lines:
        for problem_string in problem_strings:
            if problem_string in line:
                return False, f"Problem String: [{problem_string}] in [{line}]"

    return True, None


def main(args: List[str]) -> None:
    """
    Main entry point
    :param args: args from sys.argv without the name of the file
    :return: None
    """

    args = parse_args(args)
    any_problems, problem = look_for_issues(args.input_diff)

    if any_problems:
        print("No problems with diff! 😎")
    else:
        print(f"There was a problem ☹️: {problem}")


if __name__ == "__main__":
    main(sys.argv[1:])
